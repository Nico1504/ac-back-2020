const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), email  VARCHAR(255) , telefon  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10), cnp VARCHAR(20),varsta VARCHAR(3), gen VARCHAR(2))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
    gen: req.body.varsta
  };
  let error = [];
  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ






  connection.query('select * from abonamente_metrou where email="'+bilet.email+'" ', (err, result) => {
 
    if (result.length!=0){
      error.push("Emailul a fost folosit deja!");
      console.log("Emailul a fost folosit deja!");
    }})













  function isValidDate(dateString , dateString1)
{
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    var parts1 = dateString1.split("/");
    var day1 = parseInt(parts1[0], 10);
    var month1 = parseInt(parts1[1], 10);
    var year1 = parseInt(parts1[2], 10);

    if(year<year1)
       return false;
    else if(year==year1)
    {
      if(month < month1)
      return false;
      else if(month==month1)
      {
        if(day <= day1)
        return false;
      }
    }
    
    
   
};
  function isDate(input){
    var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
    if (input.match(reg)) {}
    else {
      console.log("Data este invalida va rugam folositi dd/mm/yyyy!");
      error.push("Data este invalida va rugam folositi dd/mm/yyyy!");
    }
  }
  function CheckCNP(input){
    while(input>999)
      input=input/10;
    return input;
  }
  if (!bilet.nume||!bilet.prenume||!bilet.telefon||!bilet.email||!bilet.cnp||!bilet.data_inceput||!bilet.data_sfarsit||!bilet.varsta) {
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  } else {
    if (bilet.nume.length < 2 || bilet.nume.length >= 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!bilet.nume.match("^[A-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (bilet.prenume.length < 2 || bilet.prenume.length >= 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!bilet.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (bilet.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!bilet.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (bilet.cnp.length != 13) {
      console.log("CNP-ul trebuie sa aiba 13 cifre!");
      error.push("CNP-ul trebuie sa aiba 13 cifre!");
    } else if (!bilet.cnp.match("^[0-9]+$")) {
      console.log("CNP-ul trebuie sa contina doar cifre!");
      error.push("CNP-ul trebuie sa contina doar cifre!");
    }
    if (bilet.varsta.length < 1 || bilet.varsta.length > 3) {
      console.log("Varsta trebuie sa fie intre 1 si 3 cifre!");
      error.push("Varsta trebuie sa fie intre 1 si 3 cifre!!");
    } else if (!bilet.telefon.match("^[0-9]+$")) {
      console.log("Varsta trebuie sa contina doar cifre!");
      error.push("Varsta trebuie sa contina doar cifre!");
    }
    if (!bilet.email.includes("@gmail.com") && !bilet.email.includes("@yahoo.com")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    isDate(bilet.data_inceput);
    isDate(bilet.data_sfarsit);
    var d_i = Date.parse(bilet.data_inceput);
    var d_s = Date.parse(bilet.data_sfarsit);
    if(isValidDate(bilet.data_sfarsit,bilet.data_inceput)==false){
      console.log("Data de inceput nu este inaintea celei de sfarsit!");
      error.push("Data de inceput nu este inaintea celei de sfarsit!");
    }
    var ziua_curenta = new Date();
    var data_curenta = ziua_curenta.getDate()+"/"+(ziua_curenta.getMonth()+1)+"/"+ziua_curenta.getFullYear();
    var d_c = Date.parse(data_curenta);
    if(isValidDate(data_curenta,bilet.data_inceput)==0 || isValidDate(bilet.data_sfarsit,data_curenta)==0){
      console.log("Abonament invalid!");
      error.push("Abonament invalid!");
    }

    var bla = parseInt(bilet.cnp);
    var per = parseInt(CheckCNP(bla));
    var ani = parseInt(CheckCNP(bla));
    per=per/100;
    ani=ani%100;
    var ani_tot;
    if(per == 1 || per == 2)
      ani_tot=19*100+ani;
    if(per == 3 || per == 4)
      ani_tot=18*100+ani;
    if(per == 5 || per == 6)
      ani_tot=20*100+ani;
    ani_tot=2020-ani_tot;
    if(parseInt(bilet.varsta) == parseInt(ani_tot)){
    }else{
      console.log("Varsta introdusa nu corespunde cu varsta din CNP");
      error.push("Varsta introdusa nu corespunde cu varsta din CNP");
    }
    if(per == 1 || per == 3 || per == 5)
      bilet.gen = "M";
    if(per == 2 || per == 4 || per == 6)
      bilet.gen="F";
      
  }
  
  if (error.length === 0) {
  

    const sql = `INSERT INTO abonamente_metrou (
      nume,
      prenume,
      telefon,
      cnp,
      email,
      data_inceput,
      data_sfarsit,
      varsta,
      gen) VALUES (?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.telefon,
        bilet.cnp,
        bilet.email,
        bilet.data_inceput,
        bilet.data_sfarsit,
        bilet.varsta,
        bilet.gen,
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html
